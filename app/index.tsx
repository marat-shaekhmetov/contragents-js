import * as React from "react"
import * as ReactDOM from "react-dom"
import { createStore } from "redux"
import { Provider } from "react-redux"
import mainReducer from "./reducers/index"
import AppState from "./state"
import Counterparty from "./models/Counterparty"
import Employee from "./models/Employee"
import OrgType from "./models/OrgType"

import App from "./components/app";

let initialState = new AppState();
initialState.editWindowVisible = false;
initialState.infoWindowVisible = false;
{
    let responsible = new Employee();
    responsible.id = 1;
    responsible.fullName = "Кириллов Кирил Кирилович";

    let orgType = new OrgType();
    orgType.id = 1;
    orgType.name = "OOO";

    let counterparty = new Counterparty();
    counterparty.id = 1;
    counterparty.name = "Алхимов А.А.";
    counterparty.responsible = responsible;
    counterparty.orgType = orgType;

    initialState.counterparties = [counterparty];
}

const store = createStore(mainReducer, initialState);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
