import * as React from "react";
import ContragentEditorPanelItem from "./contragent-editor-panel-item";


interface IState {
    title: string;
}

class ContragentEditorContactPanel extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        return (
            <div className="panel panel-default panel-contragent">
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                            Контактные лица
                        </a>
                    </h4>
                </div>
                <div id="collapse3" className="panel-collapse collapse">
                    <div className="panel-body">
                        <div className="well">
                            <div className="form-group">
                                <label htmlFor="newContactName">ФИО:</label>
                                <input type="text" className="form-control" id="newContactName" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="newContactPhoneNumber">Телефон:</label>
                                <input type="text" className="form-control" id="newContactPhoneNumber" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="newContactEmail">Email:</label>
                                <input type="text" className="form-control" id="newContactEmail" />
                            </div>
                            <button type="button" className="btn btn-primary">Добавить</button>
                        </div>
                        <div className="panel-group">
                            <ContragentEditorPanelItem />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContragentEditorContactPanel;