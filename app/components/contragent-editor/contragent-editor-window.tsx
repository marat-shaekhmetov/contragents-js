import * as React from "react";
import ContragentEditorMainPanel from "./contragent-editor-main-panel"
import ContragentEditorAddressPanel from "./contragent-editor-address-panel"
import ContragentEditorContactPanel from "./contragent-editor-contact-panel"
import "./contragent-editor.css"


interface IState {
    title: string;
}

class ContragentEditorWindow extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        const { title } = this.state;
        return (
            <div id="myModalBox" className="modal" style={{ display: "block" }}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 className="modal-title">Создание/редактирование контрагента</h4>
                        </div>
                        <div className="modal-body modal-body-no-padding">
                            <div className="panel-group" id="accordion">
                                <ContragentEditorMainPanel />
                                <ContragentEditorAddressPanel />
                                <ContragentEditorContactPanel />
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">Сохранить</button>
                            <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContragentEditorWindow;