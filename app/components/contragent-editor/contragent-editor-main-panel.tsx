import * as React from "react";


interface IState {
    title: string;
}

class ContragentEditorMainPanel extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        return (
            <div className="panel panel-default panel-contragent">
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            Контрагент
                        </a>
                    </h4>
                </div>
                <div id="collapse1" className="panel-collapse collapse in">
                    <div className="panel-body">
                        <div className="form-group">
                            <label htmlFor="name">Краткое наименование:</label>
                            <input type="text" className="form-control" id="name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="full_name">Полное наименование:</label>
                            <input type="text" className="form-control" id="full_name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="type">Тип юридического лица:</label>
                            <input type="text" className="form-control" id="type" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="inn">ИНН:</label>
                            <input type="text" className="form-control" id="inn" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Телефон:</label>
                            <input type="text" className="form-control" id="phone" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="mail">E-mail:</label>
                            <input type="text" className="form-control" id="mail" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContragentEditorMainPanel;