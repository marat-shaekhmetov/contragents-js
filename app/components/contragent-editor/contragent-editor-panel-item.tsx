import * as React from "react";


interface IState {
    title: string;
}

class ContragentEditorPanelItem extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h4 className="panel-title">
                        <a data-toggle="collapse" href="#collapse1">Юрдический адрес</a>
                        <a href="#" className="pull-right">
                            <span className="glyphicon glyphicon-remove glyphicon-address-action"></span>
                        </a>
                        <a href="#" className="pull-right">
                            <span className="glyphicon glyphicon-pencil glyphicon-address-action"></span>
                        </a>
                    </h4>
                </div>
                <div id="collapse1" className="panel-collapse collapse">
                    <div className="panel-body">г.Уфа Юрматинская 10</div>
                </div>
            </div>
        );
    }
}

export default ContragentEditorPanelItem;