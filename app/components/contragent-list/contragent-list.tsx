import * as React from "react"
import ContragentListTable from "./contragent-list-table"
import "./contragent-list.css"

interface IState {
    childVisible: boolean;
}

class ContragentList extends React.Component<any, IState> {
    render() {
        return (
            <div className="container">
                <h2>Контрагенты</h2>
                <button id="add-contragent" type="button" className="btn btn-default" onClick={() => this.addContragentClick()}>
                    Добавить контрагента
                </button>
                <ContragentListTable />
            </div>
        );
    }

    addContragentClick() {
        this.setState({childVisible: !this.state.childVisible});
    }
}

export default ContragentList;