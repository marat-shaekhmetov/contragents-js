import * as React from "react";
import ContragentInfoPanel from "./contragent-info-panel";
import ContragentInfoTable from "./contragent-info-table";
import ContragentInfoContact from "./contragent-info-contact";

interface IState {
    title: string;
}

class ContragentInfoWindow extends React.Component<any, IState> {

    state: IState = {
        title: "Алхимов А.А.",
    };

    render() {
        const {title} = this.state;
        return (
        <div id="myModalBox" className="modal" style = { { display: "block" }}>
        <div className="modal-dialog modal-lg">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 className="modal-title">{this.state.title}</h4>
                </div>
                <div className="modal-body">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-5">
                                <ContragentInfoPanel />
                            </div>
                            <div className="col-md-7">
                                <ContragentInfoTable />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <h3> Контактные лица </h3>
                                <ContragentInfoContact />
                                <ContragentInfoContact />
                                <ContragentInfoContact />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
        );
    }
}

export default ContragentInfoWindow;