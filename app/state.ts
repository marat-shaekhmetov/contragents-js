import Counterparty from "./models/Counterparty"
import Employee from "./models/Employee"
import OrgType from "./models/OrgType"

class AppState {
    counterparties: Array<Counterparty>;
    employees: Array<Employee>;
    orgTypes: Array<OrgType>;
    infoWindowVisible: boolean;
    editWindowVisible: boolean;
}

export default AppState;