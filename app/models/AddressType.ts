class AddressType {
    id: number;
    type: string;
}

export default AddressType;