
class Employee {
    id: number;
    fullName: string;
    phone: string;
}

export default Employee;