import OrgType from "./OrgType"
import Employee from "./Employee"
import ContactPerson from "./ContactPerson"
import Address from "./Address"

class Counterparty {
    id: number;
    name: string;
    fullName: string;
    orgType: OrgType;
    taxNumber: string;
    phone: string;
    email: string;
    responsible: Employee;
    contactPerson: Array<ContactPerson>;
    addresses: Array<string>;
}

export default Counterparty;