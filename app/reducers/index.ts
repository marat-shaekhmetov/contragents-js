import AppState from "../state"

const mainReducer = (state: any, action: any) => {
  switch (action.type) {
    case "ADD_COUNTERPARTY":
        let newState = {...{}, state};
        return newState;
    case "DEL_COUNTERPARTY": 
      let counterPartyId = action.counterparty.id;
      return state.counterparties.slice(0,counterPartyId).concat(state.slice(counterPartyId + 1));
    default:
      return state
  }
}

export default mainReducer